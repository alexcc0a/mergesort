package com.nesterov;

public class MergeSort {
    public static void mergeSort(int[] array) {
        // Если массив содержит менее двух элементов, то он уже отсортирован.
        if (array.length < 2) {
            return;
        }

        // Нфходим середину массива.
        int mid = array.length / 2;
        //Создаём два подмассива: левый и правый.
        int[] left = new int[mid];
        int[] right = new int[array.length - mid];

        // Копируем элементы из исходного массива в подмассивы.
        System.arraycopy(array, 0, left, 0, mid);
        System.arraycopy(array, mid, right, 0, array.length - mid);

        // Рекурсивно сортируем левый и правый подмассивы.
        mergeSort(left);
        mergeSort(right);

        // Объединяем отсортированные подмассивы.
        merge(array, left, right);
    }

    public static void merge(int[] array, int [] left, int[] right) {
        // Индексы для прохода по левому, правому и исходному массивам.
        int i = 0, j = 0, k = 0;

        // Сравниваем элементы из левого и правого подмассивов и помещаем их
        // в исходный массив в отсортированном порядке.
        while (i < left.length && j < right.length) {
            if (left[i] <= right[j]) {
                array[k++] = left[i++];
            } else {
                array[k++] = right[j++];
            }
        }

        // Если в левом подмассиве остались элементы, добавляем их в исходный массив.
        while (i < left.length) {
            array[k++] = array[i++];
        }

        // Если в правом подмассиве остались элементы, добавляем их в исходный массив.
        while (j < right.length) {
            array[k++] = right[j++];
        }
    }

    public static void main(String[] args) {
        // Создаём исходный массив.
        int[] array = {5, 2, 8, 1, 9, 3};

        // Вызываем метод сортировки слиянием.
        mergeSort(array);

        // Выводим отсортированный массив на экран.
        for (int num : array) {
            System.out.println(num + " ");
        }
    }
}
